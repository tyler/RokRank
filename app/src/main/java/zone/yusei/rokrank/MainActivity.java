package zone.yusei.rokrank;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.hjq.toast.CustomToast;
import com.hjq.toast.ToastStrategy;
import com.hjq.toast.ToastUtils;
import com.hjq.toast.config.IToast;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import zone.yusei.rokrank.util.Rok;

//import rok.Rok;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final int AUTO_APPLY_MSG_CODE = 51665188;
    private final int APPLY_RANK_MSG_CODE = 51885166;
    private final int CANCEL_APPLY_RANK_MSG_CODE = 51685168;
    private final int REFRESH_LIST_MSG_CODE = 51865186;

    private EditText etx, ety;
    private SharedPreferences sp;
    private int index = -1;
    private boolean isAutoApplying;
    private Button btnAuto;
    private String[] ranks  =  {"公爵", "大建筑师", "大科学家"};
    private int counter = 0;

    private Handler handler = new Handler(Looper.myLooper()) {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void handleMessage(@NonNull Message msg) {

            final int i = msg.arg1;
            super.handleMessage(msg);

            if (msg.what == AUTO_APPLY_MSG_CODE) {
                if (isAutoApplying) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Map map = new Gson().fromJson(Rok.refreshList(), Map.class);
                                Map qmap = (Map) map.get("queue");
                                if (qmap.get("set_time") != null) {
                                    int num = ((Double)((Map)((Map)map.get("info")).get("rank"+index)).get("num")).intValue();
                                    if (num == 0) {
                                        ToastUtils.show("检测到已获得" + ranks[index] + "头衔且当前申请队列为空！");
                                        Thread.sleep(2000);
                                        ToastUtils.show("10秒后重新刷新...");
                                        sendEmptyMessageDelayed(AUTO_APPLY_MSG_CODE, 10 * 1000);
                                        return;
                                    }
                                    ToastUtils.show("检测到" + ranks[index] + "头衔已获得，但申请队列人数"+num+"，开启新一轮循环！");
                                    Thread.sleep(2000);

                                    final String resultJson = Rok.applyRank(index, Integer.parseInt(etx.getText().toString()), Integer.parseInt(ety.getText().toString()));
                                    map = new Gson().fromJson(resultJson, Map.class);
                                    qmap = (Map) map.get("queue");
                                    if (map.containsKey("is_in_queue")) {
                                        ToastUtils.show(ranks[index] + "头衔申请成功！");
                                        Thread.sleep(2000);
                                    }
                                }

                                String stime  = (String) qmap.get("expect_time");
                                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                                long et = 0;
                                try {
                                    //Java 8 新添API 用于解析日期和时间
                                    et = (LocalDateTime.from(LocalDateTime.parse(stime, dateTimeFormatter))
                                            .atZone(ZoneId.systemDefault()).toInstant().getEpochSecond());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    try {
                                        et = (LocalDateTime.from(LocalDateTime.parse(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " " + stime, dateTimeFormatter))
                                                .atZone(ZoneId.systemDefault()).toInstant().getEpochSecond());
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }

                                long nt = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
                                int nextRequestSeconds = (int) (et - nt);

                                if (nextRequestSeconds < 0) {
                                    nextRequestSeconds += 86400;
                                }

                                sendEmptyMessageDelayed(AUTO_APPLY_MSG_CODE, nextRequestSeconds * 1000);
                                counter = nextRequestSeconds;

                                ToastUtils.show(ranks[index] + "头衔申请排队中...");
                                Thread.sleep(2000);
                                ToastUtils.show("当前队列人数：" + ((Double)((Map)((Map)map.get("info")).get("rank"+index)).get("num")).intValue());
                                Thread.sleep(2000);
                                ToastUtils.show("获得时间大约在：" + ((Map) map.get("queue")).get("expect_time"));
                                Thread.sleep(2000);
                                ToastUtils.show(nextRequestSeconds + "秒后尝试刷新...");
                            } catch (IOException | InterruptedException e) {
                                e.printStackTrace();
                                ToastUtils.show("自动申请失败，原因：" + e.getMessage() + "，10秒后重新尝试...");
                                sendEmptyMessageDelayed(AUTO_APPLY_MSG_CODE, 10 * 1000);
                                counter = 10;
                            }
                        }
                    }).start();
                }
            }

            if (msg.what == APPLY_RANK_MSG_CODE) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        if (isAutoApplying) {
                            ToastUtils.show("已开启自动申请模式，关闭后才可以手动申请");
                            return;
                        }

                        try {
                            // 先查询队列
                            Map map = new Gson().fromJson(Rok.refreshList(), Map.class);
                            Map qmap = (Map) map.get("queue");
                            if (qmap.get("set_time") == null) {
                                if (((Double) qmap.get("rank")).intValue() != index) {
                                    // 取消老申请再申请
                                    Rok.cancelApplyRank();
                                } else {
                                    // 提示有老申请在队列中
                                    ToastUtils.show(ranks[((Double) qmap.get("rank")).intValue()] + "申请还在排队中,"+((Map)((Map)map.get("info")).get("rank"+index)).get("num")+"，\n大约在" + qmap.get("expect_time") + "获得头衔！");
                                    return;
                                }
                            }

                            // 查询关闭情况
                            Map info = (Map) map.get("info");
                            if (((Double)((Map) info.get("rank"+index)).get("switch")).intValue() == 0) {
                                ToastUtils.show(ranks[index] + "头衔已被国王关闭申请！");
                                return;
                            }

                            // 再申请
                            final String resultJson = Rok.applyRank(index, Integer.parseInt(etx.getText().toString()), Integer.parseInt(ety.getText().toString()));
                            map = new Gson().fromJson(resultJson, Map.class);
                            if (map.containsKey("is_in_queue")) {
                                ToastUtils.show(ranks[index] + "头衔申请成功");
                                Thread.sleep(2000);
                                ToastUtils.show("当前队列人数："+ ((Double)((Map)((Map)map.get("info")).get("rank"+index)).get("num")).intValue());
                                Thread.sleep(2000);
                                ToastUtils.show("获得时间大约在" + ((Map) map.get("queue")).get("expect_time"));
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putInt("index", index);
                                editor.putString("x", etx.getText().toString().trim());
                                editor.putString("y", ety.getText().toString().trim());

                                // 保存坐标
                                editor.commit();
                            }
                        } catch (IOException | InterruptedException e) {
                            e.printStackTrace();
                            index = i;
                            e.printStackTrace();
                            ToastUtils.show(ranks[index] +"头衔申请失败，原因：" + e.getMessage());
                        }
                    }
                }).start();
            }

            if (msg.what == CANCEL_APPLY_RANK_MSG_CODE) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Rok.cancelApplyRank();
                            ToastUtils.show("已取消" + (index != -1? ranks[index] : "") + "头衔申请！");
                        } catch (IOException e) {
                            e.printStackTrace();
                            ToastUtils.show("取消" + (index != -1? ranks[index] : "") + "头衔申请失败，原因：" + e.getMessage());
                        }
                    }
                }).start();
            }

            if (msg.what == REFRESH_LIST_MSG_CODE) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final Map map = new Gson().fromJson(Rok.refreshList(), Map.class);
                            final Map qmap = (Map) map.get("queue");
                            StringBuilder tips = new StringBuilder();
                            tips.append("当前公爵队列人数："+ ((Double)((Map)((Map)map.get("info")).get("rank0")).get("num")).intValue() +"\n");
                            tips.append("当前建筑师队列人数："+ ((Double)((Map)((Map)map.get("info")).get("rank1")).get("num")).intValue() +"\n");
                            tips.append("当前科学家队列人数："+ ((Double)((Map)((Map)map.get("info")).get("rank2")).get("num")).intValue() +"\n");
                            if (qmap.get("set_time") == null) {
                                tips.append(ranks[((Double) qmap.get("rank")).intValue()] + "申请还在排队中...\n");
                                tips.append("获得时间大约在" + ((Map) map.get("queue")).get("expect_time"));
                            } else {
                                tips.append(ranks[((Double) qmap.get("rank")).intValue()] + "头衔已于" + qmap.get("set_time") + "获得！\n");
                            }
                            ToastUtils.show(tips);
                        } catch (IOException e) {
                            e.printStackTrace();
                            ToastUtils.show("获取" + (index != -1? ranks[index] : "") + "头衔申请状态失败，原因：" + e.getMessage());
                        }
                    }
                }).start();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ToastUtils.init(getApplication(), new ToastStrategy() {

            @Override
            public IToast createToast(Application application) {
                IToast toast = super.createToast(application);
                if (toast instanceof CustomToast) {
                    CustomToast customToast = ((CustomToast) toast);
                    // 设置短 Toast 的显示时长（默认是 2000 毫秒）
                    customToast.setShortDuration(5000);
                    // 设置长 Toast 的显示时长（默认是 3500 毫秒）
                    customToast.setLongDuration(5000);
                }
                return toast;
            }
        });

        sp = getSharedPreferences("position.xml", MODE_PRIVATE);
        index = sp.getInt("index", -1);
        etx = findViewById(R.id.main_etx);
        etx.setText(sp.getString("x", "0"));
        ety = findViewById(R.id.main_ety);
        ety.setText(sp.getString("y", "0"));

        findViewById(R.id.main_btn_0).setOnClickListener(this);
        findViewById(R.id.main_btn_1).setOnClickListener(this);
        findViewById(R.id.main_btn_2).setOnClickListener(this);
        findViewById(R.id.main_btn_3).setOnClickListener(this);
        findViewById(R.id.main_btn_4).setOnClickListener(this);
        btnAuto = findViewById(R.id.main_btn_5);
        btnAuto.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_btn_0: {
                Message message = handler.obtainMessage();
                message.arg1 = index;
                index = 0;
                message.what = APPLY_RANK_MSG_CODE;
                handler.sendMessage(message);
            } break;
            case R.id.main_btn_1: {
                Message message = handler.obtainMessage();
                message.arg1 = index;
                index = 1;
                message.what = APPLY_RANK_MSG_CODE;
                handler.sendMessage(message);
            } break;
            case R.id.main_btn_2: {
                Message message = handler.obtainMessage();
                message.arg1 = index;
                index = 2;
                message.what = APPLY_RANK_MSG_CODE;
                handler.sendMessage(message);
            } break;
            case R.id.main_btn_3: {
                Message message = handler.obtainMessage();
                message.what = CANCEL_APPLY_RANK_MSG_CODE;
                handler.sendMessage(message);
            } break;
            case R.id.main_btn_4: {
                Message message = handler.obtainMessage();
                message.what = REFRESH_LIST_MSG_CODE;
                handler.sendMessage(message);
            } break;
            case R.id.main_btn_5:
                if (index != -1) {
                    if (!isAutoApplying) {
                        isAutoApplying = true;
                        btnAuto.setText("自动循环申请" + ranks[index] +  "中...");
                        Message message = handler.obtainMessage();
                        message.what = AUTO_APPLY_MSG_CODE;
                        handler.sendMessage(message);
                    } else {
                        isAutoApplying = false;
                        btnAuto.setText("自动申请");
                    }
                } else {
                    ToastUtils.show("开启自动申请失败，未查询到上一次申请头衔！");
                }
                break;
        }
    }
}
package zone.yusei.rokrank.util;

import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Rok {
    public static String applyRank(int flag, int x, int y) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.Companion.parse("application/x-www-form-urlencoded");

        RequestBody body = RequestBody.Companion.create("rank="+flag+"&area=111&kvk==&intx="+x+"&inty="+y+"&pw=123456&openid=o2VxB5RIaF6dsrr0besvaesRxqtY&nickname=%E4%B9%9D%E5%A4%A9%E5%9B%BE%E8%85%BE&avatarurl=https%3A%2F%2Fthirdwx.qlogo.cn%2Fmmopen%2Fvi_32%2F8FiaBahb1F25sZlPK6sYPZteOgFQuhnNLAdDQj4MZpWPC4ErNcxAFEI2NPM35EcLpy6A3wtnu425X2NicVSV43dg%2F132",
                mediaType);
        Request request = new Request.Builder()
                .url("https://wg.aigames.vip/cntx2/up_apply_data.php")
                .method("POST", body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("User-Agent", "wechat")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String  applyDukeRank(int x, int y) throws IOException {
        return applyRank(0, x, y);
    }

    public static String  applyArchitectRank(int x, int y) throws IOException {
        return applyRank(0, x, y);
    }

    public static String  applyScientistRank(int x, int y) throws IOException {
        return applyRank(0, x, y);
    }

    public static String refreshList() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("https://wg.aigames.vip/cntx2/get_is_in_list.php?openid=o2VxB5RIaF6dsrr0besvaesRxqtY")
                .method("GET", null)
                .addHeader("User-Agent", "wechat")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static void cancelApplyRank() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.Companion.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.Companion.create("openid=o2VxB5RIaF6dsrr0besvaesRxqtY&used=4", mediaType);
        Request request = new Request.Builder()
                .url("https://wg.aigames.vip/cntx2/modify_apply_used.php")
                .method("POST", body)
                .addHeader("User-Agent", "wechat")
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();
        client.newCall(request).execute();
    }
}

package rok

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/tidwall/gjson"
)

func ApplyDukeRank(x, y string) bool {
	return ApplyRank("0", x, y)
}

func ApplyArchitectRank(x, y string) bool {
	return ApplyRank("1", x, y)
}

func ApplyScientistRank(x, y string) bool {
	return ApplyRank("2", x, y)
}

func ApplyRank(flag, x, y string) string {
	url := "https://wg.aigames.vip/cntx2/up_apply_data.php"
	method := "POST"

	payload := strings.NewReader("rank=" + flag + "&area=111&kvk%3D=&intx=" + x + "&inty=" + y + "&pw=123456&openid=o2VxB5RIaF6dsrr0besvaesRxqtY&nickname=%25E4%25B9%259D%25E5%25A4%25A9%25E5%259B%25BE%25E8%2585%25BE&avatarurl=https%253A%252F%252Fthirdwx.qlogo.cn%252Fmmopen%252Fvi_32%252F8FiaBahb1F25sZlPK6sYPZteOgFQuhnNLAdDQj4MZpWPC4ErNcxAFEI2NPM35EcLpy6A3wtnu425X2NicVSV43dg%252F132")

	log.Println(flag, x, y)

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return false, ""
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("User-Agent", "wechat")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return false, ""
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return false, ""
	}
	
}

func CancelApplyRank() {
	url := "https://wg.aigames.vip/cntx2/modify_apply_used.php"
	method := "POST"

	payload := strings.NewReader("openid=o2VxB5RIaF6dsrr0besvaesRxqtY&used=4")

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return
	}
	req.Header.Add("User-Agent", "wechat")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()
}

func HasGotRank() bool {
	url := "https://wg.aigames.vip/cntx2/get_is_in_list.php?openid=o2VxB5RIaF6dsrr0besvaesRxqtY"
	method := "GET"

	client := &http.Client{}
	req, _ := http.NewRequest(method, url, nil)

	req.Header.Add("User-Agent", "wechat")

	res, _ := client.Do(req)
	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	value := gjson.Get(string(body), "is_in_queue")
	if value.Exists() {
		return value.Bool()
	}

	return false
}

func GetRankStatus() string {
	url := "https://wg.aigames.vip/cntx2/get_is_in_list.php?openid=o2VxB5RIaF6dsrr0besvaesRxqtY"
	method := "GET"

	client := &http.Client{}
	req, _ := http.NewRequest(method, url, nil)

	req.Header.Add("User-Agent", "wechat")

	res, _ := client.Do(req)
	defer res.Body.Close()

	body, _ := ioutil.ReadAll(res.Body)

	ranks := []string{"公爵", "大建筑师", "大科学家"}

	value := gjson.Get(string(body), "queue.set_time")
	if value.Exists() {
		if value.Value() == nil {
			return fmt.Sprintf("%s还在排队中，大约%s获得头衔!",
				ranks[gjson.Get(string(body), "queue.rank").Int()],
				gjson.Get(string(body), "queue.expect_time").String())
		} else {
			return fmt.Sprintf("%s已于%s获得!",
				ranks[gjson.Get(string(body), "queue.rank").Int()],
				gjson.Get(string(body), "queue.set_time").String())
		}
	}

	return "请求失败！"
}
